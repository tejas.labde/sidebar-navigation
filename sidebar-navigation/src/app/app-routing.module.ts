import { SamplePageThreeComponent } from './sample-page-three/sample-page-three.component';
import { SamplePageTwoComponent } from './sample-page-two/sample-page-two.component';
import { SamplePageOneComponent } from './sample-page-one/sample-page-one.component';
import { TestPageComponent } from './test-page/test-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path:'', component:TestPageComponent, children:[
    {path:'page-one',component:SamplePageOneComponent},
    {path:'page-two',component:SamplePageTwoComponent},
    {path:'page-three',component:SamplePageThreeComponent},
  ]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
